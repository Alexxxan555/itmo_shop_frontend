import  Vue from 'vue'
import  Router from 'vue-router'


import vContact from '../components/v-contact'
import vCatalogXiaomi from '../components/v-catalog-xiaomi'
import vCatalogSamsung from '../components/v-catalog-samsung'
import vCatalogApple from '../components/v-catalog-apple'
import vMainPage from '../components/v-main-page'
import vCatalog from '../components/v-catalog'
import vCart from '../components/v-cart'

Vue.use(Router);

let router = new Router( {
    routes: [
        {
            path:'/',
            name: 'mainPage',
            component: vMainPage
        },
        {
            path:'/catalogApple',
            name: 'catalogApple',
            component: vCatalogApple
        },
        {
            path:'/catalogSamsung',
            name: 'catalogSamsung',
            component: vCatalogSamsung
        },
        {
            path: '/catalogXiaomi',
            name: 'catalogXiaomi',
            component: vCatalogXiaomi
        },
        {
            path: '/contact',
            name: 'contact',
            component: vContact
        },
        {
            path: '/',
            name: 'catalog',
            component: vCatalog
        },
        {
            path: '/cart',
            name: 'cart',
            component: vCart,
            props: true
        },

    ]

})

export default router;